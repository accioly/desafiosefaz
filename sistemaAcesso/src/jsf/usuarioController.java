package jsf;

import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import jpa.daos.TelefoneDAO;
import jpa.daos.UsuarioDAO;
import jpa.entities.Telefone;
import jpa.entities.Usuario;

@ManagedBean(name = "userCtrl")
@ViewScoped
public class usuarioController {
	
	private UsuarioDAO usuarioDAO = new UsuarioDAO();
	private TelefoneDAO telefoneDAO = new TelefoneDAO();
	private Usuario usuario = new Usuario();
	private List<Telefone> telefones = new ArrayList<Telefone>(0);
		
	public void cadastrarUsuario(){
		usuarioDAO.inserirUsuario(usuario);
		this.usuario = new Usuario();
	}
	
	public void editarUsuario(){
		usuarioDAO.alterarUsuario(usuario);
		this.usuario = new Usuario();
	}
	
	public void removerUsuario(){
		usuarioDAO.deletarUsuario(usuario);
		this.usuario = new Usuario();
	}
	
	public UsuarioDAO getUsuarioDAO() {
		return usuarioDAO;
	}
	public void setUsuarioDAO(UsuarioDAO usuarioDAO) {
		this.usuarioDAO = usuarioDAO;
	}
	public TelefoneDAO getTelefoneDAO() {
		return telefoneDAO;
	}
	public void setTelefoneDAO(TelefoneDAO telefoneDAO) {
		this.telefoneDAO = telefoneDAO;
	}
	public Usuario getUsuario() {
		return usuario;
	}
	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	public List<Telefone> getTelefones() {
		return telefones;
	}
	public void setTelefones(List<Telefone> telefones) {
		this.telefones = telefones;
	}


}

package jsf;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import jpa.daos.UsuarioDAO;
import jpa.entities.Usuario;

@ManagedBean(name = "LoginCtrl")
@ViewScoped
public class loginController {
	private UsuarioDAO usuarioDAO = new UsuarioDAO();
	private Usuario usuario = new Usuario();
	
	@PostConstruct
	public void init(){
		
	}
	
	public loginController(){
		
	}
	
	public String logar() {

		usuario = usuarioDAO.buscarPorLogin(usuario.getLoginUsuario());
		if (usuario == null) {
			usuario = new Usuario();
			FacesContext.getCurrentInstance().addMessage(null,
					new FacesMessage(FacesMessage.SEVERITY_ERROR, "Login Inexistente!", "Erro no Login!"));
			return null;
		} else {
			usuario = usuarioDAO.getUsuario(usuario.getLoginUsuario(), usuario.getSenha());    	 
			if (usuario == null) {
				usuario = new Usuario();
				FacesContext.getCurrentInstance().addMessage(null,
						new FacesMessage(FacesMessage.SEVERITY_ERROR, "Senha Invalida!", "Erro no Login!"));
				return null;
			} else {
				return "/main";
			}
		}          
	}
	
	public String cadastrarUsuario(){
		return "/cadastroUsuario";
	}
	
	public String inserir(){
		usuarioDAO.inserirUsuario(usuario);
		this.usuario = new Usuario();
		return "/index";
	}

	public String logoff(){
		this.usuario = new Usuario();
		return "/index";
	}
	
	public String voltar(){
		return "/index";
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
}

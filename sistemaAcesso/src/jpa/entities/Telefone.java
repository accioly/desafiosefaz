package jpa.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="telefone")
public class Telefone {
	
	@Id
	@Column(name="telefone_id", unique=true)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer telefoneId;
	
	@Column(name="ddd", nullable=false, unique=false)
	private Integer ddd;
	
	@Column(name="numero", nullable=false, unique=false)
	private String numero;
	
	@Column(name="tipo", nullable=false, unique=false)
	private String tipo;
	
    @ManyToOne
    @JoinColumn(name = "id")
	private Usuario usuario;
		
	public Integer getTelefoneId() {
		return telefoneId;
	}

	public void setTelefoneId(Integer telefoneId) {
		this.telefoneId = telefoneId;
	}
	
	public Integer getDdd() {
		return ddd;
	}
	
	public void setDdd(Integer ddd) {
		this.ddd = ddd;
	}
	
	public String getNumero() {
		return numero;
	}
	
	public void setNumero(String numero) {
		this.numero = numero;
	}
		
	public String getTipo() {
		return tipo;
	}
	
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

}

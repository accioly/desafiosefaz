package jpa.daos;

import javax.ejb.Local;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.NoResultException;
import javax.persistence.Persistence;

import jpa.entities.Usuario;

@Local
public class UsuarioDAO {


	protected EntityManagerFactory factory = Persistence
			.createEntityManagerFactory("PersistenceUnit-SistemaAcesso");

	protected EntityManager em = factory.createEntityManager();


	public Usuario getUsuario(String login, String senha) {

		try {
			Usuario usuario = (Usuario) em
					.createQuery("SELECT u from Usuario u where u.loginUsuario = :loginUsuario and u.senha = :senha")
					.setParameter("loginUsuario", login)
					.setParameter("senha", senha).getSingleResult();

			return usuario;
		} catch (NoResultException e) {
			return null;
		}

	}

	public boolean inserirUsuario(Usuario usuario) {
		try {
			em.getTransaction().begin();
			em.persist(usuario);
			em.getTransaction().commit();
			return true;
		} catch (Exception e) {
			em.getTransaction().rollback();
			e.printStackTrace();
			return false;
		}
	}

	public boolean alterarUsuario(Usuario usuario){
		try {
			em.getTransaction().begin();
			em.merge(usuario);
			em.getTransaction().commit();
			return true;
		} catch (Exception e) {
			em.getTransaction().rollback();
			e.printStackTrace();
			return false;
		}

	}

	public boolean deletarUsuario(Usuario usuario) {
		try {
			em.getTransaction().begin();
			em.remove(usuario);
			em.getTransaction().commit();
			return true;
		} catch (Exception e) {
			em.getTransaction().rollback();
			e.printStackTrace();
			return false;
		}
	}

	public Usuario buscarPorLogin(String login) {
		if(login != null && !login.equals("")){
			try {
				Usuario usuario = (Usuario) em
						.createQuery("SELECT u from Usuario u where u.loginUsuario = :loginUsuario")
						.setParameter("loginUsuario", login).getSingleResult();;
						return usuario; 
			} catch (NoResultException e) {
				return null;
			}
		}
		return null;
	}

}

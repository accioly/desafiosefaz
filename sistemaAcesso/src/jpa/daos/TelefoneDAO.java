package jpa.daos;

import java.util.List;

import javax.ejb.Local;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.NoResultException;
import javax.persistence.Persistence;

import jpa.entities.Telefone;

@Local
public class TelefoneDAO {
	private EntityManagerFactory factory = Persistence
			.createEntityManagerFactory("PersistenceUnit-SistemaAcesso");
	private EntityManager em = factory.createEntityManager();
	
	public Telefone getTelefone(String ddd, String numero) {

		try {
			Telefone telefone = (Telefone) em
					.createQuery("SELECT t from Telefone t where t.ddd = :ddd and t.numero = :numero")
					.setParameter("ddd", ddd)
					.setParameter("numero", numero).getSingleResult();

			return telefone;
			
		} catch (NoResultException e) {
			return null;
		}

	}

	public boolean inserirTelefone(Telefone telefone) {
		try {
			em.getTransaction().begin();
			em.persist(telefone);
			em.getTransaction().commit();
			return true;
		} catch (Exception e) {
			em.getTransaction().rollback();
			e.printStackTrace();
			return false;
		}
	}
	
	public boolean alterarTelefone(Telefone telefone) {
		try {
			em.getTransaction().begin();
			em.merge(telefone);
			em.getTransaction().commit();
			return true;
		} catch (Exception e) {
			em.getTransaction().rollback();
			e.printStackTrace();
			return false;
		}
	}

	public boolean deletarTelefone(Telefone telefone) {
		try {
			em.getTransaction().begin();
			em.remove(telefone);
			em.getTransaction().commit();
			return true;
		} catch (Exception e) {
			em.getTransaction().rollback();
			e.printStackTrace();
			return false;
		}
	}

	@SuppressWarnings("unchecked")
	public List<Telefone> buscarPorLoginUsuario(String login) {
		if(login != null && !login.equals("")){
			try {
				List<Telefone> list = (List<Telefone>) em
						.createQuery("SELECT t from Telefone t where t.usuario.loginUsuario = :login")
						.setParameter("login", login).getResultList();
						return list; 
			} catch (NoResultException e) {
				return null;
			}
		}
		return null;
	}

}
